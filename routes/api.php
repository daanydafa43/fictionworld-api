<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\PesananController;
use App\Http\Controllers\PesananDetailController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\KeranjangListController;

Route::post('/login', [AuthController::class, 'cekUser']);
Route::post('/logout', [AuthController::class, 'logout']);
Route::post('/register', [AuthController::class, 'create']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/buku', [BukuController::class, 'index']);
Route::get('/bukubygenre/{id}', [BukuController::class, 'getBukubyGenre']);
Route::get('/buku/{id}', [BukuController::class, 'show']);
Route::post('/tambah-buku', [BukuController::class, 'store']);
Route::put('/edit-buku/{id}', [BukuController::class, 'update']);
Route::delete('/buku/{id}', [BukuController::class, 'destroy']);

Route::get('/genre', [GenreController::class, 'index']);
Route::get('/genre/{id}', [GenreController::class, 'show']);

Route::get('/pesanan', [PesananController::class, 'index']);
Route::get('/pesanan/{id}', [PesananController::class, 'show']);
Route::post('/pesanan', [PesananController::class, 'store']);
Route::put('/pesanan/{id}', [PesananController::class, 'updateStatus']);
Route::delete('/pesanan/{id}', [PesananController::class, 'destroy']);

Route::get('/riwayatpesanan', [PesananController::class, 'getUserOrders']);
Route::get('/mypesanan', [PesananController::class, 'getUserOrders']);

Route::get('/pesanandetail', [PesananDetailController::class, 'index']);
Route::get('/pesanandetail/{id}', [PesananDetailController::class, 'show']);

Route::get('/user', [UserController::class, 'index']);
Route::get('/user/{id}', [UserController::class, 'show']);
Route::post('/user', [UserController::class, 'store']);
Route::put('/user/{id}', [UserController::class, 'update']);
Route::delete('/user/{id}', [UserController::class, 'destroy']);
Route::post('/set-admin/{user}', [UserController::class, 'setAdmin']);

Route::get('/keranjang', [KeranjangListController::class, 'index']);
Route::put('/keranjang/{id}', [KeranjangListController::class, 'update']);
Route::post('/keranjang', [KeranjangListController::class, 'store']);
Route::delete('/keranjang/{id}', [KeranjangListController::class, 'destroy']);
