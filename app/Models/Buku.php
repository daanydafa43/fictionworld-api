<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    use HasFactory;
    protected $guarded = 'buku_id';
    protected $primaryKey = 'buku_id';

    public function genre()
    {
        return $this->belongsTo(Genre::class, 'genre_id', 'genre_id');
    }

    public function pesananDetails()
    {
        return $this->hasMany(PesananDetail::class, 'buku_id', 'buku_id');
    }

    public function keranjangList()
    {
        return $this->hasMany(KeranjangList::class, 'buku_id', 'buku_id');
    }

    
}
