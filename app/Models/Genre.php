<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $guarded = 'genre_id';
    protected $primaryKey = 'genre_id';

    public function Buku()
    {
        return $this->hasMany(Buku::class, 'buku_id', 'buku_id');
    }
}