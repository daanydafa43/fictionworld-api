<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Buku;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function cekUser(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $user = User::where('email', $email)->first();

        if (!is_null($user)) {
            if (password_verify($password, $user->password)) {
                // Di sini, Anda bisa menghasilkan token atau sesuatu untuk otentikasi API
                return response()->json(['message' => 'Login successful'], 200);
            } else {
                return response()->json(['error' => 'Incorrect email or password'], 401);
            }
        } else {
            return response()->json(['error' => 'Account not found'], 404);
        }
    }

    public function logout()
    {
        // Di sini, Anda bisa melakukan proses logout sesuai kebutuhan API
        return response()->json(['message' => 'Logout successful'], 200);
    }

    public function indexAdmin()
    {
        $data = [
            'bukus' => Buku::all()
        ];
        return response()->json($data, 200);
    }

    public function getAllUser()
    {
        $data = [
            'users' => User::all(),
        ];
        return response()->json($data, 200);
    }

    public function setIsAdmin($id)
    {
        $setAdmin = User::find($id);
        if ($setAdmin->user_type != 'admin') {
            $setAdmin->user_type = 'admin';
        } else {
            $setAdmin->user_type = 'user';
        }
        $setAdmin->save();
        return response()->json(['success' => 'Credential successfully updated'], 200);
    }

    public function destroy($id)
    {
        User::destroy($id);
        return response()->json(['success' => 'User successfully deleted'], 200);
    }

    public function create(Request $request)
    {
        $inputUser = new User;

        $inputUser->name = $request->input('name');
        $inputUser->email = $request->input('email');
        $inputUser->password = $request->input('password');
        $inputUser->user_type = $request->input('user_type', 'user');

        $inputUser->save();
        return response()->json(['success' => 'Registration successful'], 200);
    }
}
