<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Buku;
use App\Models\Genre;

class BukuController extends Controller
{
    public function index(Request $request)
    {
        $query = Buku::query();

        if ($request->has('search')) {
            $titleSearch = $request->input('search');
            $query->where('title', 'LIKE', "%{$titleSearch}%");
        }

        $bukus = $query->get();
        return response()->json($bukus);
    }

    public function getBukubyGenre(Request $request, $genreID)
    {
        $genre = Genre::where('genre_id', $genreID)->pluck('genre')->first();

        $query = Buku::where('genre_id', $genreID);

        if ($request->has('search')) {
            $titleSearch = $request->input('search');
            $query->where('title', 'LIKE', "%{$titleSearch}%");
        }

        $bukus = $query->get();

        return response()->json([
            'genre' => $genre,
            'bukus' => $bukus,
        ]);
    }

    public function show($id)
    {
        $buku = Buku::find($id);
        $genre = Genre::where('genre_id', $buku->genre_id)->pluck('genre')->first();
        if ($buku) {
            return response()->json([
                'genre' => $genre,
                'bukus' => $buku,
            ]);
        } else {
            return response()->json(['message' => 'Buku not found'], 404);
        }
    }

    public function store(Request $request)
    {
        $cover = $request->file('cover');

        // new Buku to store request
        $inputBuku = new Buku;

        $inputBuku->title = $request->input('title');
        $inputBuku->author = $request->input('author');
        $inputBuku->synopsis = $request->input('synopsis');
        $inputBuku->genre_id = $request->input('genre_id');
        $inputBuku->price = $request->input('price');
        $inputBuku->cover = 'defaultimg.jpg';
        if ($cover != null) {
            $inputBuku->cover = $cover->getClientOriginalName();
            $tujuan_upload = 'img/';
            $cover->move($tujuan_upload, $cover->getClientOriginalName());
        }

        if ($inputBuku->save()) {
            return response()->json(['message' => 'Buku berhasil ditambahkan'], 201);
        } else {
            return response()->json(['message' => 'Gagal menambahkan buku'], 400);
        }
    }


    public function update(Request $request, $id)
    {
        $buku = Buku::find($id);

        if (!$buku) {
            return response()->json(['message' => 'Buku not found'], 404);
        }

        $namaCover = $request->hasFile('cover') ? $request->file('cover')->getClientOriginalName() : $request->input('old-cover');

        $data = [
            'title' => $request->input('title'),
            'author' => $request->input('author'),
            'synopsis' => $request->input('synopsis'),
            'cover' => $namaCover,
            'genre_id' => $request->input('genre_id'),
            'price' => $request->input('price'),
        ];
        //dd($data);
        if (Buku::where('buku_id', $id)->update($data)) {
            return response()->json(['message' => 'Buku berhasil diubah'], 201);
        } else {
            return response()->json(['message' => 'Buku gagal diubah'], 400);
            $request->file('cover')->getError();
        }
    }

    public function destroy($id)
    {
        $buku = Buku::find($id);
        if ($buku) {
            $buku->delete();
            return response()->json(['message' => 'Buku deleted']);
        } else {
            return response()->json(['message' => 'Buku not found'], 404);
        }
    }
}
