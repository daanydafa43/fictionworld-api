<?php

namespace App\Http\Controllers;

use App\Models\Pesanan;
use App\Models\PesananDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PesananController extends Controller
{
    public function index()
    {
        $pesanan = Pesanan::all();
        return response()->json($pesanan);
    }

    public function show($id)
    {
        $pesanan = Pesanan::find($id);
        if ($pesanan) {
            return response()->json($pesanan);
        } else {
            return response()->json(['message' => 'Pesanan not found'], 404);
        }
    }

    public function store(Request $request)
    {
        $pesanan = Pesanan::create([
            'user_id' => $request->input('user_id'),
            'total_amount' => $request->input('total'),
            'status' => "pending",

        ]);

        foreach ($request->input('bukus') as $buku) {
            PesananDetail::create([
                'pesanan_id' => $pesanan->pesanan_id,
                'buku_id' => $buku['buku_id'],
                'qty' => $buku['quantity'],
            ]);
        }

        return response()->json([
            'message' => 'Pesanan berhasil dibuat',
            'pesanan' => $pesanan,
        ], 201);
    }

    public function getUserOrders(Request $request)
    {
        $user_id = $request->input('user_id');

        $pesananData = Pesanan::where('user_id', $user_id)->get();

        return response()->json([
            'success' => true,
            'data' => $pesananData,
        ]);
    }

    // public function getUserOrders(Request $request)
    // {
    //     try {
    //         $user_id = $request->input('user_id');

    //         // Ambil semua pesanan berdasarkan user_id
    //         $pesanan = Pesanan::where('user_id', $user_id)->get();

    //         $orders = [];
    //         foreach ($pesanan as $order) {
    //             // Ambil detail pesanan berdasarkan pesanan_id
    //             $details = PesananDetail::where('pesanan_id', $order->pesanan_id)->get();

    //             var_dump($details);

    //             $orderDetails = [];
    //             foreach ($details as $detail) {
    //                 // Ambil informasi buku berdasarkan buku_id
    //                 $buku = $detail->buku()->first();

    //                 $orderDetails[] = [
    //                     'title' => $buku->title,
    //                     'price' => $buku->price,
    //                     'quantity' => $detail->qty,
    //                 ];
    //             }

    //             $orders[] = [
    //                 'pesanan_id' => $order->pesanan_id,
    //                 'total_amount' => $order->total,
    //                 'details' => $orderDetails,
    //             ];
    //         }

    //         return response()->json([
    //             'status' => 'success',
    //             'data' => $user_id,
    //         ], 200);
    //     } catch (\Exception $e) {
    //         return response()->json([
    //             'status' => 'error',
    //             'message' => $e->getMessage(),
    //         ], 500);
    //     }
    // }

    public function updateStatus(Request $request, $id)
    {
        $status = $request->input('status');

        $pesanan = Pesanan::find($id);
        if ($pesanan) {
            $pesanan->update(['status' => $status]);
            return response()->json(['message' => 'Quantity updated successfully'], 200);
        } else {
            return response()->json(['message' => 'Pesanan not found'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $pesanan = Pesanan::find($id);
        if ($pesanan) {
            $pesanan->update($request->all());
            return response()->json($pesanan);
        } else {
            return response()->json(['message' => 'Pesanan not found'], 404);
        }
    }

    public function destroy($id)
    {
        $pesanan = Pesanan::find($id);
        if ($pesanan) {
            $pesanan->delete();
            return response()->json(['message' => 'Pesanan deleted']);
        } else {
            return response()->json(['message' => 'Pesanan not found'], 404);
        }
    }

    public function adminShow($userId)
    {
        // Pastikan pengguna sudah terautentikasi
        if (Session::has('user_id')) {
            // Dapatkan pesanan berdasarkan user_id
            $pesanan = Pesanan::where('user_id', $userId)->get();

            // Pastikan pesanan ditemukan
            if ($pesanan) {
                // Kembalikan pesanan dalam format JSON
                return response()->json(['pesanans' => $pesanan], 200);
            } else {
                // Pesanan tidak ditemukan
                return response()->json(['error' => 'Pesanan tidak ditemukan'], 404);
            }
        } else {
            // Pengguna belum terautentikasi, kembalikan respons error
            return response()->json(['error' => 'Unauthenticated'], 401);
        }
    }
}
