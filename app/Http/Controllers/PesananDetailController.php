<?php

namespace App\Http\Controllers;

use App\Models\PesananDetail;
use Illuminate\Http\Request;

class PesananDetailController extends Controller
{
    public function index()
    {
        $pesananDetails = PesananDetail::all();
        return response()->json($pesananDetails);
    }

    public function show($id)
    {
        $pesananDetail = PesananDetail::where('pesanan_id', $id)->get();
        if ($pesananDetail) {
            return response()->json($pesananDetail);
        } else {
            return response()->json(['message' => 'PesananDetail not found'], 404);
        }
    }

    public function store(Request $request)
    {
        $pesananDetail = PesananDetail::create($request->all());
        return response()->json($pesananDetail, 201);
    }

    public function update(Request $request, $id)
    {
        $pesananDetail = PesananDetail::find($id);
        if ($pesananDetail) {
            $pesananDetail->update($request->all());
            return response()->json($pesananDetail);
        } else {
            return response()->json(['message' => 'PesananDetail not found'], 404);
        }
    }

    public function destroy($id)
    {
        $pesananDetail = PesananDetail::find($id);
        if ($pesananDetail) {
            $pesananDetail->delete();
            return response()->json(['message' => 'PesananDetail deleted']);
        } else {
            return response()->json(['message' => 'PesananDetail not found'], 404);
        }
    }
}
