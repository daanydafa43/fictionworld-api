<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KeranjangList;

class KeranjangListController extends Controller
{
    public function index(Request $request)
    {
        $userId = $request->input('user_id');

        $items = KeranjangList::where('user_id', $userId)->get();

        return response()->json([
            'success' => true,
            'data' => $items,
        ]);
    }

    public function store(Request $request)
    {
        $userId = $request->input('user_id');
        $bukuId = $request->input('buku_id');
        $qty = $request->input('qty');

        // Cek apakah kombinasi user_id dan buku_id sudah ada di database
        $keranjangItem = KeranjangList::where('user_id', $userId)
            ->where('buku_id', $bukuId)
            ->first();

        if ($keranjangItem) {
            // Jika sudah ada, tambahkan quantity
            KeranjangList::where('user_id', $userId)
                ->where('buku_id', $bukuId)
                ->update(['qty' => $keranjangItem->qty + $qty]);
        } else {
            // Jika belum ada, buat entry baru
            KeranjangList::create([
                'user_id' => $userId,
                'buku_id' => $bukuId,
                'qty' => $qty,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Item berhasil ditambahkan ke keranjang',
        ]);
    }

    public function update(Request $request)
    {
        $userId = $request->input('user_id');
        $bukuId = $request->input('buku_id');
        $qty = $request->input('qty');

        $keranjangItem = KeranjangList::where('user_id', $userId)
            ->where('buku_id', $bukuId)
            ->first();

        if ($keranjangItem) {
            // Jika sudah ada, tambahkan quantity
            KeranjangList::where('user_id', $userId)
                ->where('buku_id', $bukuId)
                ->update(['qty' => $keranjangItem->qty + $qty]);
            return response()->json(['message' => 'Quantity updated successfully'], 200);
        } else {
            return response()->json(['message' => 'Item not found'], 404);
        }
    }

    public function destroy(Request $request)
    {
        $userId = $request->input('user_id');
        $bukuId = $request->input('buku_id');

        $deleted = KeranjangList::where('user_id', $userId)
            ->where('buku_id', $bukuId)
            ->delete();

        if ($deleted) {
            return response()->json(['message' => 'Item deleted successfully'], 200);
        }
        return response()->json(['message' => 'Item not found'], 404);
    }
}
