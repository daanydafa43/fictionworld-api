<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bukus', function (Blueprint $table) {
            $table->id('buku_id');
            $table->string('title');
            $table->string('author');
            $table->text('synopsis');
            $table->string('cover');
            $table->foreignId('genre_id');
            $table->double('price');
            $table->timestamps();

            $table->foreign('genre_id')->references('genre_id')->on('genres')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bukus');
    }
};
