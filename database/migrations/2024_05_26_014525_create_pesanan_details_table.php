<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pesanan_details', function (Blueprint $table) {
            $table->unsignedBigInteger('pesanan_id');
            $table->unsignedBigInteger('buku_id');
            $table->integer('qty');
            $table->timestamps();

            $table->foreign('pesanan_id')->references('pesanan_id')->on('pesanans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('buku_id')->references('buku_id')->on('bukus')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pesanan_details');
    }
};
