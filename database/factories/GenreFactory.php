<?php

namespace Database\Factories;

use App\Models\Genre;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Genre>
 */
class GenreFactory extends Factory
{
    protected $model = Genre::class;

    public function definition()
    {
        return [
            'genre_id' => $this->faker->unique()->numberBetween(1, 1000), // Sesuaikan sesuai dengan kebutuhan
            'genre' => $this->faker->word,
        ];
    }
}
