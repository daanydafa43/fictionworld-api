<?php

namespace Database\Factories;

use App\Models\Buku;
use App\Models\Genre;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Buku>
 */
class BukuFactory extends Factory
{
    protected $model = Buku::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence,
            'author' => $this->faker->name,
            'synopsis' => $this->faker->paragraph,
            'cover' => $this->faker->imageUrl(640, 480, 'books', true),
            'genre_id' => Genre::factory(),
            'price' => $this->faker->randomFloat(2, 10, 100),
        ];
    }
}
