<?php
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Buku;
use App\Models\Genre;


class BukuTest extends TestCase
{
    use WithFaker;
    // use RefreshDatabase;

    public function test_buku_index()
    {
        Buku::factory()->count(3)->create();

        $response = $this->get('/api/buku');

        $response->assertStatus(200);
    }

    public function test_buku_show()
    {
        $buku = Buku::factory()->create();

        $response = $this->get("/api/buku/{$buku->buku_id}");

        $response->assertStatus(200);
    }

    public function test_buku_store()
    {
        $genre = Genre::factory()->make();
        $genre->save();

        $data = [
            'title' => $this->faker->sentence,
            'author' => $this->faker->name,
            'synopsis' => $this->faker->paragraph,
            'cover' => $this->faker->imageUrl(640, 480, 'books', true),
            'genre_id' => $genre->genre_id,
            'price' => $this->faker->randomFloat(2, 10, 100),
        ];

        $response = $this->post('/api/buku', $data);

        $response->assertStatus(201);
    }

    public function test_buku_update()
    {
        $buku = Buku::factory()->create();

        $data = [
            'title' => 'Updated Title',
            'author' => 'Updated Author',
        ];

        $response = $this->put("/api/buku/{$buku->buku_id}", $data);

        $response->assertStatus(200);
    }

    public function test_buku_destroy()
    {
        $buku = Buku::factory()->create();

        $response = $this->delete("/api/buku/{$buku->buku_id}");

        $response->assertStatus(200);
    }
}
